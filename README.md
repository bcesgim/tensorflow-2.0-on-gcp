# Tensorflow 2.0  on GCP 


## Goals

* Set-up Notebook on AI Platform with TensorFlow 2.0 image
* Enable APIs for Compute Engine, BigQuery, and Cloud ML Engine
* Stage preprocessed data using BigQuery
* Prepare data for model input
* Defined a logistic regression model
* Train it on approximately 2M rows using tf.keras
* Call predictions from the trained model
* Evaluate model to SavedModel files
* Deploy model versions to AI Platform